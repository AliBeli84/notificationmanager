﻿using System;

namespace NotificationManager.Domain
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class DtoForAttribute : Attribute
    {
        public Type EntityClass { get; set; }

        public DtoForAttribute(Type _EntityClass)
        {
            this.EntityClass = _EntityClass;
        }

        public DtoForAttribute() { }
    }
}
