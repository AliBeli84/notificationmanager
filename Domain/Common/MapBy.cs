﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace NotificationManager.Domain.Common
{
    public class MapBy<TViewModel>
    {
        private List<Expression<Func<TViewModel, object>>> xAllExpressions = new List<Expression<Func<TViewModel, object>>>();

        public MapBy(params Expression<Func<TViewModel, object>>[] expressions) => xAllExpressions.AddRange(expressions);

        public void AddMapBy(Expression<Func<TViewModel, object>> expression) => xAllExpressions.Add(expression);

        public void ClearAllMaps() => xAllExpressions.Clear();

        public IEnumerable<string> GetIncludeProperties()
        {
            List<string> result = new List<string>();
            foreach (var xExpression in xAllExpressions)
            {
                var xSplitedByDot = xExpression.Body.ToString().Split('.').Where(w => w != "And()").Skip(1);
                if (xSplitedByDot.Any(a => a.Contains("Select(")))
                    throw new Exception("به جای سلکت از اند استفاده کنید");

                result.Add(string.Join(".", xSplitedByDot));
            }
            return result;
        }
    }

    public static class SelectHelper
    {
        public static T And<T>(this IEnumerable<T> enumerable) => enumerable.First();
    }

    public enum MapByType
    {
        stringInclude,
        expression
    }
}
