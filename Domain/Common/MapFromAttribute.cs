﻿using System;

namespace NotificationManager.Domain
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class MapFromAttribute : Attribute
    {
        public string PropertyName { get; set; }

        public MapFromAttribute() { }

        public MapFromAttribute(string _PropertyName)
        {
            this.PropertyName = _PropertyName;
        }
    }
}
