﻿using System.Collections.Generic;

namespace NotificationManager.Domain.Common
{
    public class PagingResult<T>
    {
        public IEnumerable<T> Data { get; set; }
        public long Count { get; set; }
    }
}
