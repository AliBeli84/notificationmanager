﻿namespace NotificationManager.Domain.Constants
{
    public static class ApiUrlHelper
    {
        public static string SendBulkSms => "sms/send/bulk2";
        public static string SmsStatus => "sms/status";
    }
}
