﻿using System;

namespace NotificationManager.Domain.Constants
{
    public static class CacheHelper
    {
        public readonly static DateTimeOffset ExpireDate = DateTimeOffset.Now.AddMinutes(5);
        public static string SmsProvider(string provider) => $"SmsProvider-{provider}";
    }
}
