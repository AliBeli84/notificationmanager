﻿using System;

namespace NotificationManager.Domain.Entities
{
    #region == IEntity Interface ==
    public interface IEntity { }

    public interface IEntity<T>
    {
        T Id { get; set; }
    }
    #endregion

    #region == BaseEntity ==
    public abstract class BaseEntity<T> : IEntity<T>, IEntity
    {
        public T Id { get; set; }
    }
    #endregion

    #region == Other Interface ==
    public interface ISoftDelete
    {
        bool IsDeleted { get; set; }
    }

    public interface IActive
    {
        bool IsActive { get; set; }
    }

    public interface IRegister<TRegistererID>
    {
        TRegistererID RegistererID { get; set; }
        DateTime RegisterDate { get; set; }
    }

    public interface IModifier<TModifierID> where TModifierID : struct
    {
        Nullable<TModifierID> ModifierID { get; set; }
        DateTime? ModifyDate { get; set; }
    }
    #endregion
}
