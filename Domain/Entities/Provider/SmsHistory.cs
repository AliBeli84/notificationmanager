﻿using System;

namespace NotificationManager.Domain.Entities.Provider
{
    public class SmsHistory : BaseEntity<long>
    {
        #region == Fields ==
        public string Provider { get; set; }

        public string Message { get; set; }

        public string MsgId { get; set; }

        public string PhoneNumbers { get; set; }

        public bool Status { get; set; }

        public DateTime SendDate { get; set; }
        #endregion
    }
}
