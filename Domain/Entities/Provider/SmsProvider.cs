﻿using System;

namespace NotificationManager.Domain.Entities.Provider
{
    public class SmsProvider : BaseEntity<short>
    {
        #region == Fields ==
        public string Provider { get; set; }

        public string Apikey { get; set; }

        public string BaseUrl { get; set; }

        public string SenderPhone { get; set; }

        public DateTime RegisterDate { get; set; }
        #endregion
    }
}
