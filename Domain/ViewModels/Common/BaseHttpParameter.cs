﻿using NotificationManager.Domain.ViewModels.SmsVM;
using System;
using System.Collections.Generic;
using System.Text;

namespace NotificationManager.Domain.ViewModels.Common
{
    public class BaseHttpParameter
    {
        public BaseHttpParameter(string url, string apiKey)
        {
            SmsGroup = new SmsGroup();
            SmsStatus = new SmsStatus();
            this.Url = url;
            this.ApiKey = apiKey;
        }

        public string Url { get; set; }
        public string ApiKey { get; set; }
        public string SenderPhone { get; set; }
        public SmsGroup SmsGroup { get; set; }
        public SmsStatus SmsStatus { get; set; }
    }
}
