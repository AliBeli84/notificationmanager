﻿namespace NotificationManager.Domain.ViewModels.Common
{
    /// <summary>
    /// APi Response From HttpClient Request
    /// </summary>
    public class HttpGeneralResponse
    {
        public HttpGeneralResponse(HttpBaseResponse response, bool success)
        {
            Success = success;
            Response = response;
        }

        public bool Success { get; set; }
        public HttpBaseResponse Response { get; set; }
    }

    public class HttpBaseResponse
    {
        public HttpBaseResponse(bool status, string message,object data)
        {
            Status = status;
            Message = message;
            Data = data;
        }

        public bool Status { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
