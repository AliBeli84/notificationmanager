﻿using NotificationManager.Domain.ViewModels.Common;

namespace NotificationManager.Domain.ViewModels.SmsVM
{
    public class SmsGroup
    {
        public string Provider { get; set; }
        public string PhoneNumbers { get; set; }
        public string Text { get; set; }
    }


}
