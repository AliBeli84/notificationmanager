﻿using NotificationManager.Domain.Entities.Provider;
using System;
using System.Collections.Generic;
using System.Text;

namespace NotificationManager.Domain.ViewModels.SmsVM
{
    public class SmsHistoryParameter
    {
        public bool Status { get; set; }
    }

    [DtoFor(typeof(SmsHistory))]
    public class SmsHistoryResult
    {
        public string Message { get; set; }

        public string PhoneNumbers { get; set; }

        public bool Status { get; set; }

        public string MsgId { get; set; }

        public DateTime SendDate { get; set; }
    }
}
