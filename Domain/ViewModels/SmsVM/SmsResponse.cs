﻿using System.Collections.Generic;

namespace NotificationManager.Domain.ViewModels.SmsVM
{
    public class SmsResponse
    {
        public bool Status { get; set; }

        public string Message { get; set; }

        public object Data { get; set; }
    }

    public class SmsGroupMessageIds
    {
        public List<int> MessageIds { get; set; }
    }

    public class EmptyResponse { }
}
