﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NotificationManager.Domain.ViewModels.SmsVM
{
    public class SmsStatus
    {
        public string MessageIds { get; set; }

        public string Provider { get; set; }
    }
}
