﻿using Microsoft.AspNetCore.Mvc;
using NotificationManager.Api.Shared;
using NotificationManager.Domain.ViewModels.SmsVM;
using NotificationManager.Services.Service.Provider;
using System.Threading.Tasks;

namespace NotificationManager.Api.Controllers
{
    public class SmsController : ApiControllerBase
    {
        #region == Ctor ==
        private readonly ISmsProviderService _smsProviderService;

        public SmsController(ISmsProviderService smsProviderService)
        {
            _smsProviderService = smsProviderService;
        }
        #endregion

        //! In Moredo Be Dalil Swagger Comment kardam Va Az Ravashse Zir Estefade Kardam

        //[HttpGet("GetSmsHistory")]
        //public async Task<GeneralResult> GetSmsHistory(SmsHistoryParameter smsHistoryParameter)
        //{
        //    return Ok(await _smsProviderService.GetHistory(smsHistoryParameter));
        //}

        [HttpGet("GetSmsHistory/{status}")]
        public async Task<GeneralResult> GetSmsHistory(bool status)
        {
            return Ok(await _smsProviderService.GetHistory(status));
        }

        [HttpPost("SendParsaSmsGroup")]
        public async Task<GeneralResult> SendParsaSmsGroup(SmsGroup smsGroup)
        {
            return Ok(await _smsProviderService.SendGroup(smsGroup));
        }

        [HttpPost("GetSmsStatus")]
        public async Task<GeneralResult> GetSmsStatus(SmsStatus smsStatus)
        {
            return Ok(await _smsProviderService.GetSmsStatus(smsStatus));
        }
    }
}
