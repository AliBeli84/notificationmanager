using NotificationManager.Api.Middlewares;
using NotificationManager.Domain.Mapper;
using NotificationManager.Domain.ViewModels.Common;
using NotificationManager.Persistence.Context;
using NotificationManager.Persistence.Repositories;
using NotificationManager.Services.Interface.Repository;
using NotificationManager.Services.Interface.Service;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NotificationManager.Services.Service.Common;

namespace NotificationManager.Api
{
    public class Startup
    {
        #region == Ctor ==
        public IConfiguration _configuration { get; }
        private readonly IHostEnvironment Environment;

        public Startup(IConfiguration configuration, IHostEnvironment environment)
        {
            _configuration = configuration;
            Environment = environment;

        }
        #endregion

        #region == Configuration ==
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddHttpClient();
            services.AddMapperConfigurations();
            services.AddServices();
            services.AddSingleton<IHttpService, HttpService>();

            services.AddDbContext<ApplicationDBContext>(opt =>
            {
                opt.UseSqlServer(_configuration.GetConnectionString("ApplicationConnection"));
            });
            services.AddLazyCache();

            services.AddMvcCore(opt => opt.EnableEndpointRouting = false)
             .AddApiExplorer()
             .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
             .AddNewtonsoftJson(options =>
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddSwaggerGen();
            services.AddResponseCaching();

            services.AddCors();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });
        }

        public void Configure(IApplicationBuilder app, IHostEnvironment env)
        {
            //IdentityModelEventSource.ShowPII = true;
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseCors(i => i.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.AddExceptionHandling();
            app.UseResponseCaching();
            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "api/{controller}/{action}/{id?}");
            });
        }
        #endregion
    }
}
