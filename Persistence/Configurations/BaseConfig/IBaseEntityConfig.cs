﻿using Microsoft.EntityFrameworkCore;

namespace NotificationManager.Persistence.Configurations
{
    public interface IBaseEntityConfig<T> : IEntityTypeConfiguration<T> where T : class { }
}
