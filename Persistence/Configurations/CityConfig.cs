﻿using MadPay.Domain.Entities.Countrya;
using MadPay.Persistence.Configurations.BaseConfig;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MadPay.Persistence.Configurations.Country
{

    public class CityConfig : IBaseEntityConfig<City>
    {
        public void Configure(EntityTypeBuilder<City> builder)
        {
            //Fields
            builder.HasKey(k => k.Id);

            builder.HasOne(i => i.Province).WithMany(p => p.Cities).HasForeignKey(i => i.ProvinceId);

            //Table
            builder.ToTable("City", "Geo");
        }
    }
}
