﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NotificationManager.Domain.Entities.Provider;
using NotificationManager.Persistence.Configurations.BaseConfig;
using System;
using System.Collections.Generic;
using System.Text;

namespace NotificationManager.Persistence.Configurations.ProviderConfiguration
{
    public class SmsHistoryConfig : IBaseEntityConfig<SmsHistory>
    {
        public void Configure(EntityTypeBuilder<SmsHistory> builder)
        {
            //Fields
            builder.HasKey(k => k.Id);
            builder.Property(i => i.Provider).HasColumnType("NVARCHAR(200)").IsRequired();
            builder.Property(i => i.Message).HasColumnType("NVARCHAR(2000)").IsRequired();
            builder.Property(i => i.PhoneNumbers).HasColumnType("VARCHAR(20)").IsRequired();
            builder.Property(i => i.MsgId).HasColumnType("VARCHAR(50)").IsRequired();
            builder.Property(i => i.SendDate).HasDefaultValueSql("getdate()");

            //Table
            builder.ToTable(nameof(SmsHistory), SchemaConfig.Provider);
        }
    }
}
