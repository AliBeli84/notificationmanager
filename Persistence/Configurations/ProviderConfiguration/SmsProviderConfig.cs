﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NotificationManager.Domain.Entities.Provider;
using NotificationManager.Persistence.Configurations.BaseConfig;

namespace NotificationManager.Persistence.Configurations.ProviderConfiguration
{
    public class SmsProviderConfig : IBaseEntityConfig<SmsProvider>
    {
        public void Configure(EntityTypeBuilder<SmsProvider> builder)
        {
            //Fields
            builder.HasKey(k => k.Id);
            builder.Property(i => i.Provider).HasColumnType("NVARCHAR(200)").IsRequired();
            builder.Property(i => i.Apikey).HasColumnType("VARCHAR(200)").IsRequired();
            builder.Property(i => i.BaseUrl).HasColumnType("VARCHAR(200)").IsRequired();
            builder.Property(i => i.SenderPhone).HasColumnType("VARCHAR(50)").IsRequired();
            builder.Property(i => i.RegisterDate).HasDefaultValueSql("getdate()");

            //Table
            builder.ToTable(nameof(SmsProvider), SchemaConfig.Provider);
        }
    }
}
