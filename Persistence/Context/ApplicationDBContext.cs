﻿using NotificationManager.Domain.Entities;
using NotificationManager.Persistence.Configurations;
using NotificationManager.Persistence.Context.Extensions;
using Microsoft.EntityFrameworkCore;

namespace NotificationManager.Persistence.Context
{
    public class ApplicationDBContext : DbContext
    {
        #region == Ctor ==
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options) { }

        public ApplicationDBContext() : base() { }
        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.AddDBSetFromModel(typeof(IEntity).Assembly, typeof(IEntity));
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(IBaseEntityConfig<>).Assembly);
        }
    }
}
