﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NotificationManager.Persistence.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Provider");

            migrationBuilder.CreateTable(
                name: "SmsHistory",
                schema: "Provider",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Provider = table.Column<string>(type: "NVARCHAR(200)", nullable: false),
                    Message = table.Column<string>(type: "NVARCHAR(2000)", nullable: false),
                    MsgId = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    PhoneNumbers = table.Column<string>(type: "VARCHAR(20)", nullable: false),
                    Status = table.Column<bool>(nullable: false),
                    SendDate = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SmsHistory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SmsProvider",
                schema: "Provider",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Provider = table.Column<string>(type: "NVARCHAR(200)", nullable: false),
                    Apikey = table.Column<string>(type: "VARCHAR(200)", nullable: false),
                    BaseUrl = table.Column<string>(type: "VARCHAR(200)", nullable: false),
                    SenderPhone = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    RegisterDate = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SmsProvider", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SmsHistory",
                schema: "Provider");

            migrationBuilder.DropTable(
                name: "SmsProvider",
                schema: "Provider");
        }
    }
}
