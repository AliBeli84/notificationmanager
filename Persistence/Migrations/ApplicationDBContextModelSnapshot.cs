﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using NotificationManager.Persistence.Context;

namespace NotificationManager.Persistence.Migrations
{
    [DbContext(typeof(ApplicationDBContext))]
    partial class ApplicationDBContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.6")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("NotificationManager.Domain.Entities.Provider.SmsHistory", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Message")
                        .IsRequired()
                        .HasColumnType("NVARCHAR(2000)");

                    b.Property<string>("MsgId")
                        .IsRequired()
                        .HasColumnType("VARCHAR(50)");

                    b.Property<string>("PhoneNumbers")
                        .IsRequired()
                        .HasColumnType("VARCHAR(20)");

                    b.Property<string>("Provider")
                        .IsRequired()
                        .HasColumnType("NVARCHAR(200)");

                    b.Property<DateTime>("SendDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("datetime2")
                        .HasDefaultValueSql("getdate()");

                    b.Property<bool>("Status")
                        .HasColumnType("bit");

                    b.HasKey("Id");

                    b.ToTable("SmsHistory","Provider");
                });

            modelBuilder.Entity("NotificationManager.Domain.Entities.Provider.SmsProvider", b =>
                {
                    b.Property<short>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("smallint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Apikey")
                        .IsRequired()
                        .HasColumnType("VARCHAR(200)");

                    b.Property<string>("BaseUrl")
                        .IsRequired()
                        .HasColumnType("VARCHAR(200)");

                    b.Property<string>("Provider")
                        .IsRequired()
                        .HasColumnType("NVARCHAR(200)");

                    b.Property<DateTime>("RegisterDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("datetime2")
                        .HasDefaultValueSql("getdate()");

                    b.Property<string>("SenderPhone")
                        .IsRequired()
                        .HasColumnType("VARCHAR(50)");

                    b.HasKey("Id");

                    b.ToTable("SmsProvider","Provider");
                });
#pragma warning restore 612, 618
        }
    }
}
