﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using NotificationManager.Domain.Common;
using NotificationManager.Domain.Entities;
using NotificationManager.Persistence.Context;
using NotificationManager.Services.Interface.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace NotificationManager.Persistence.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class, IEntity
    {
        #region == Ctor ==
        internal ApplicationDBContext _context;
        private readonly DbSet<TEntity> _dbSet;
        private readonly IMapper _mapper;

        public BaseRepository(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            this._dbSet = _context.Set<TEntity>();
            this._mapper = mapper;
        }
        #endregion

        #region == Sync Function ==

        #region == Read ==
        public virtual IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            MapBy<TEntity> includeProperties = null)
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            if (includeProperties != null)
            {
                foreach (var item in includeProperties.GetIncludeProperties())
                    query = query.Include(item);
            }

            if (orderBy != null)
                return orderBy(query).AsNoTracking();
            else
                return query.AsNoTracking();
        }

        public virtual IQueryable<TEntity> Query(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            MapBy<TEntity> includeProperties = null)
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            if (includeProperties != null)
            {
                foreach (var item in includeProperties.GetIncludeProperties())
                    query = query.Include(item);
            }

            if (orderBy != null)
                query = orderBy(query);

            return query;
        }

        public virtual IEnumerable<TEntity> Get(
            ref int listCount,
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            MapBy<TEntity> includeProperties = null,
            int page = 0,
            int pageSize = 0)
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            if (listCount != -1)
                listCount = query.Count();

            if (includeProperties != null)
            {
                foreach (var item in includeProperties.GetIncludeProperties())
                    query = query.Include(item);
            }

            if (orderBy != null)
                query = orderBy(query);

            if (page != 0 && pageSize != 0)
                query = query.Skip((page - 1) * pageSize).Take(pageSize);

            return query.AsNoTracking();
        }

        public virtual IEnumerable<TViewModel> Get<TViewModel>(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            MapBy<TViewModel> includeProperties = null)
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            if (includeProperties == null)
                includeProperties = new MapBy<TViewModel>();

            if (orderBy != null)
                return orderBy(query).AsNoTracking().ProjectTo<TViewModel>(_mapper.ConfigurationProvider, null, includeProperties.GetIncludeProperties().ToArray());
            else
                return query.AsNoTracking().ProjectTo<TViewModel>(_mapper.ConfigurationProvider, null, includeProperties.GetIncludeProperties().ToArray());
        }

        public virtual IEnumerable<TViewModel> Get<TViewModel>(
            ref int listCount,
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            MapBy<TViewModel> includeProperties = null,
            int page = 0,
            int pageSize = 0)
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            if (listCount != -1)
                listCount = query.Count();

            if (orderBy != null)
                query = orderBy(query);

            if (includeProperties == null)
                includeProperties = new MapBy<TViewModel>();

            if (page != 0 && pageSize != 0)
                query = query.Skip((page - 1) * pageSize).Take(pageSize);

            return query.AsNoTracking().ProjectTo<TViewModel>(_mapper.ConfigurationProvider, null, includeProperties.GetIncludeProperties().ToArray());
        }

        public virtual IEnumerable<TEntity> Get<TValue>(
            ref int listCount,
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            Expression<Func<TEntity, TValue>> groupBy = null,
            int take = 0,
            int limit = 0) where TValue : struct
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            if (listCount != -1)
                listCount = query.Count();

            if (orderBy != null)
                query = orderBy(query);

            if (groupBy != null)
                query = query.GroupBy(groupBy).SelectMany(p => p.Take(take));

            if (limit != 0)
                query = query.Take(limit);

            return query.AsNoTracking();
        }

        public virtual IEnumerable<TViewModel> Get<TValue, TViewModel>(
            ref int listCount,
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            Expression<Func<TEntity, TValue>> groupBy = null,
            int take = 0,
            int limit = 0) where TValue : struct
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            if (listCount != -1)
                listCount = query.Count();

            if (orderBy != null)
                query = orderBy(query);

            if (groupBy != null)
                query = query.GroupBy(groupBy).SelectMany(p => p.Take(take));

            if (limit != 0)
                query = query.Take(limit);

            return query.AsNoTracking().ProjectTo<TViewModel>(_mapper.ConfigurationProvider);
        }

        public virtual TEntity GetByID(object id)
        {
            return _dbSet.Find(id);
        }

        public virtual TViewModel GetFirst<TViewModel>(Expression<Func<TEntity, bool>> wherePredicate, MapBy<TViewModel> includeProperties = null)
        {
            if (includeProperties == null)
                includeProperties = new MapBy<TViewModel>();

            return _dbSet.Where(wherePredicate).ProjectTo<TViewModel>(_mapper.ConfigurationProvider, null, includeProperties.GetIncludeProperties().ToArray())
                         .FirstOrDefault();

        }

        public virtual bool Any(Expression<Func<TEntity, bool>> filter = null)
        {
            IQueryable<TEntity> query = _dbSet;
            if (filter != null)
                return query.Any(filter);

            return query.Any();
        }
        #endregion

        #region == CUD ==
        public virtual void Insert(TEntity entity)
        {
            _dbSet.Add(entity);
        }

        public virtual void Delete(object id)
        {
            TEntity entityToDelete = _dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if (_context.Entry(entityToDelete).State == EntityState.Detached)
                _dbSet.Attach(entityToDelete);

            _dbSet.Remove(entityToDelete);
        }

        public virtual void Update(TEntity entityToUpdate, IEnumerable<Expression<Func<TEntity, object>>> ExcludePropertiesFromUpdate = null)
        {
            _dbSet.Attach(entityToUpdate);
            _context.Entry(entityToUpdate).State = EntityState.Modified;

            foreach (var xCurExcludeColumn in ExcludePropertiesFromUpdate ?? new List<Expression<Func<TEntity, object>>>())
                _context.Entry(entityToUpdate).Property(xCurExcludeColumn).IsModified = false;
        }

        public virtual void UpdateProperty<TResult>(TEntity xBaseEntity, params Expression<Func<TEntity, IEnumerable<TResult>>>[] relations) where TResult : class
        {
            var xEntry = _dbSet.Attach(xBaseEntity);
            xEntry.State = EntityState.Unchanged;

            foreach (var relation in relations)
            {
                var newData = xEntry.Collection(propertyExpression: relation).CurrentValue.ToList();
                xEntry.Collection(propertyExpression: relation).CurrentValue = null;

                //remove trackers:
                foreach (var i in newData)
                    _context.Entry(i).State = EntityState.Detached;

                // get fresh data from db
                xEntry.Collection(propertyExpression: relations.First()).Load();
                xEntry.Collection(propertyExpression: relations.First()).CurrentValue = newData;
            }
        }
        #endregion

        #endregion

        #region == AsyncFunctions ==
        public virtual async Task<List<TEntity>> GetAsync(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            MapBy<TEntity> includeProperties = null)
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            if (includeProperties != null)
            {
                foreach (var item in includeProperties.GetIncludeProperties())
                    query = query.Include(item);
            }

            if (orderBy != null)
                return await orderBy(query).AsNoTracking().ToListAsync();
            else
                return await query.AsNoTracking().ToListAsync();
        }

        public virtual async Task<List<TViewModel>> GetAsync<TViewModel>(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            MapBy<TViewModel> includeProperties = null)
        {

            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }
            if (includeProperties == null)
                includeProperties = new MapBy<TViewModel>();

            if (orderBy != null)
            {
                return await orderBy(query).AsNoTracking().ProjectTo<TViewModel>(_mapper.ConfigurationProvider, null, includeProperties.GetIncludeProperties().ToArray()).ToListAsync();
            }
            else
            {
                return await query.AsNoTracking().ProjectTo<TViewModel>(_mapper.ConfigurationProvider, null, includeProperties.GetIncludeProperties().ToArray()).ToListAsync();
            }
        }

        public virtual async Task<PagingResult<TEntity>> GetAsync(
            bool needDBCount,
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            MapBy<TEntity> includeProperties = null,
            int page = 0,
            int pageSize = 0)
        {
            IQueryable<TEntity> query = _dbSet;
            PagingResult<TEntity> pagingGenericResult = new PagingResult<TEntity>();

            if (filter != null)
                query = query.Where(filter);

            if (needDBCount)
                pagingGenericResult.Count = query.Count();

            if (includeProperties != null)
            {
                foreach (var item in includeProperties.GetIncludeProperties())
                    query = query.Include(item);
            }

            if (orderBy != null)
                query = orderBy(query);

            if (page != 0 && pageSize != 0)
                query = query.Skip((page - 1) * pageSize).Take(pageSize);

            pagingGenericResult.Data = await query.AsNoTracking().ToListAsync();
            return pagingGenericResult;
        }

        public virtual async Task<PagingResult<TViewModel>> GetAsync<TViewModel>(
            bool needDBCount,
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            MapBy<TViewModel> includeProperties = null,
            int page = 0,
            int pageSize = 0)
        {
            IQueryable<TEntity> query = _dbSet;
            PagingResult<TViewModel> pagingGenericResult = new PagingResult<TViewModel>();

            if (filter != null)
                query = query.Where(filter);

            if (needDBCount)
                pagingGenericResult.Count = query.Count();

            if (orderBy != null)
                query = orderBy(query);

            if (includeProperties == null)
                includeProperties = new MapBy<TViewModel>();

            if (page != 0 && pageSize != 0)
                query = query.Skip((page - 1) * pageSize).Take(pageSize);

            pagingGenericResult.Data = await query.AsNoTracking().ProjectTo<TViewModel>(_mapper.ConfigurationProvider, null, includeProperties.GetIncludeProperties().ToArray()).ToListAsync();
            return pagingGenericResult;
        }

        public virtual async Task<PagingResult<TEntity>> GetAsync<TValue>(
            bool needDBCount,
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            Expression<Func<TEntity, TValue>> groupBy = null,
            int take = 0,
            int limit = 0) where TValue : struct
        {
            PagingResult<TEntity> pagingGenericResult = new PagingResult<TEntity>();
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            if (needDBCount)
                pagingGenericResult.Count = query.Count();

            if (orderBy != null)
                query = orderBy(query);

            if (groupBy != null)
                query = query.GroupBy(groupBy).SelectMany(p => p.Take(take));

            if (limit != 0)
                query = query.Take(limit);

            pagingGenericResult.Data = await query.AsNoTracking().ToListAsync();
            return pagingGenericResult;
        }

        public virtual async Task<PagingResult<TViewModel>> GetAsync<TValue, TViewModel>(
            bool needDBCount,
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            Expression<Func<TEntity, TValue>> groupBy = null,
            int take = 0,
            int limit = 0) where TValue : struct
        {
            PagingResult<TViewModel> pagingGenericResult = new PagingResult<TViewModel>();
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            if (needDBCount)
                pagingGenericResult.Count = query.Count();

            if (orderBy != null)
                query = orderBy(query);

            if (groupBy != null)
                query = query.GroupBy(groupBy).SelectMany(p => p.Take(take));

            if (limit != 0)
                query = query.Take(limit);

            pagingGenericResult.Data = await query.AsNoTracking().ProjectTo<TViewModel>(_mapper.ConfigurationProvider).ToListAsync();
            return pagingGenericResult;
        }

        public virtual async Task<TEntity> GetByIDAsync(object id)
        {
            return await _dbSet.FindAsync(id);
        }

        public virtual async Task<TViewModel> GetFirstAsync<TViewModel>(Expression<Func<TEntity, bool>> wherePredicate, MapBy<TViewModel> includeProperties = null)
        {
            if (includeProperties == null)
                includeProperties = new MapBy<TViewModel>();

            return await _dbSet.Where(wherePredicate).ProjectTo<TViewModel>(_mapper.ConfigurationProvider, null, includeProperties.GetIncludeProperties().ToArray())
                               .FirstOrDefaultAsync();
        }

        public virtual async Task<bool> AnyAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            IQueryable<TEntity> query = _dbSet;
            if (filter != null)
                return await query.AnyAsync(filter);

            return await query.AnyAsync();
        }

        public async Task InsertAsync(TEntity entity)
        {
            await _dbSet.AddAsync(entity);
        }

        public async Task DeleteAsync(object id)
        {
            TEntity entityToDelete = await _dbSet.FindAsync(id);
            Delete(entityToDelete);
        }
        #endregion

        #region == Other Function ==
        private List<PropertyInfo> GetSystemType(IEnumerable<PropertyInfo> xPropertyInfoList)
        {
            List<PropertyInfo> xSystemProperty = new List<PropertyInfo>();
            foreach (var xProperty in xPropertyInfoList)
            {
                string xTypeName = "";
                if (xProperty.PropertyType.IsGenericType)
                {
                    Type xGenericType = xProperty.PropertyType.GetGenericArguments().FirstOrDefault();
                    if (xGenericType != null)
                        xTypeName = xGenericType.FullName;
                    else
                        continue;
                }
                else
                    xTypeName = xProperty.PropertyType.FullName;

                bool xIsSystemType = xTypeName.StartsWith("System", StringComparison.OrdinalIgnoreCase);

                if (xIsSystemType)
                    xSystemProperty.Add(xProperty);
            }
            return xSystemProperty;
        }

        private List<PropertyInfo> GetNullType(IEnumerable<PropertyInfo> xPropertyList, object xEntity)
        {
            List<PropertyInfo> xNullValueProperty = new List<PropertyInfo>();
            object obj = (object)xEntity;
            foreach (var xProperty in xPropertyList)
            {
                object xPropertyValue = xProperty.GetValue(obj, null);
                var xPropertyType = xProperty.PropertyType;
                var xDefaultValue = GetDefaultValue(xPropertyType);

                if (xPropertyValue == null || xPropertyValue.Equals(xDefaultValue))
                    xNullValueProperty.Add(xProperty);
            }
            return xNullValueProperty;
        }

        private object GetDefaultValue(Type xType)
        {
            if (xType.IsValueType && Nullable.GetUnderlyingType(xType) == null)
                return Activator.CreateInstance(xType);
            else
                return null;
        }
        #endregion
    }
}
