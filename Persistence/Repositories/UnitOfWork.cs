﻿using AutoMapper;
using NotificationManager.Persistence.Context;
using NotificationManager.Services.Interface.Repository;
using System;
using System.Threading.Tasks;
using NotificationManager.Domain.Entities.Provider;

namespace NotificationManager.Persistence.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        #region == Ctor ==
        private readonly ApplicationDBContext _context;
        IMapper _mapper;

        public UnitOfWork(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        } 
        #endregion

        #region == Init Repository ==
        private IBaseRepository<SmsProvider> _smsProviderRepository;
        private IBaseRepository<SmsHistory> _smsHistoryRepository;
        #endregion

        #region == Declare Repository ==

        #region == Providers ==
        public IBaseRepository<SmsProvider> SmsProviderRepository => _smsProviderRepository ??= new BaseRepository<SmsProvider>(_context, _mapper);
        public IBaseRepository<SmsHistory> SmsHistoryRepository => _smsHistoryRepository ??= new BaseRepository<SmsHistory>(_context, _mapper);
        #endregion

        #endregion

        #region == Base ==
        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        protected virtual void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
