﻿using NotificationManager.Domain.Common;
using NotificationManager.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace NotificationManager.Services.Interface.Repository
{
    public interface IBaseRepository<TEntity> where TEntity : class, IEntity
    {
        #region == Sync Fuction ==

        #region == Read ==
        IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            MapBy<TEntity> includeProperties = null);

        IQueryable<TEntity> Query(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            MapBy<TEntity> includeProperties = null);

        IEnumerable<TEntity> Get(
            ref int listCount,
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            MapBy<TEntity> includeProperties = null,
            int page = 0,
            int pageSize = 0);

        IEnumerable<TViewModel> Get<TViewModel>(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            MapBy<TViewModel> includeProperties = null);

        IEnumerable<TViewModel> Get<TViewModel>(
            ref int listCount,
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            MapBy<TViewModel> includeProperties = null,
            int page = 0,
            int pageSize = 0);

        TEntity GetByID(object id);

        TViewModel GetFirst<TViewModel>(
            Expression<Func<TEntity, bool>> wherePredicate,
            MapBy<TViewModel> includeProperties = null);

        bool Any(Expression<Func<TEntity, bool>> filter = null);

        #endregion

        #region == CUD ==
        void Insert(TEntity entity);

        void Update(TEntity entityToUpdate, IEnumerable<Expression<Func<TEntity, object>>> excludePropertiesFromUpdate = null);

        void UpdateProperty<TResult>(TEntity xBaseEntity, params Expression<Func<TEntity, IEnumerable<TResult>>>[] xRelations) where TResult : class;

        void Delete(object id);

        void Delete(TEntity entityToDelete);
        #endregion

        #endregion

        #region == ASync Fuction ==

        #region == Read ==
        Task<List<TEntity>> GetAsync(
             Expression<Func<TEntity, bool>> filter = null,
             Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
             MapBy<TEntity> includeProperties = null);

        Task<PagingResult<TEntity>> GetAsync(
            bool needDBCount,
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            MapBy<TEntity> includeProperties = null,
            int page = 0,
            int pageSize = 0);

        Task<List<TViewModel>> GetAsync<TViewModel>(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            MapBy<TViewModel> includeProperties = null);

        Task<PagingResult<TViewModel>> GetAsync<TViewModel>(
            bool needDBCount,
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            MapBy<TViewModel> includeProperties = null,
            int page = 0,
            int pageSize = 0);

        Task<TEntity> GetByIDAsync(object id);

        Task<TViewModel> GetFirstAsync<TViewModel>(Expression<Func<TEntity, bool>> xWherePredicate, MapBy<TViewModel> includeProperties = null); 
        #endregion

        #region == CUD ==
        Task InsertAsync(TEntity entity);

        Task DeleteAsync(object id); 
        #endregion

        #endregion
    }
}
