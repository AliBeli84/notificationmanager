﻿using NotificationManager.Domain.Entities.Provider;
using System;
using System.Threading.Tasks;

namespace NotificationManager.Services.Interface.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        #region == Providers ==
        IBaseRepository<SmsProvider> SmsProviderRepository { get; }
        IBaseRepository<SmsHistory> SmsHistoryRepository { get; }
        #endregion

        #region == Base ==
        void SaveChanges();
        Task SaveChangesAsync();
        #endregion
    }
}
