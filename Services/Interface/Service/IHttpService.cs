﻿using NotificationManager.Domain.ViewModels.Common;
using NotificationManager.Services.Service.HttpParameters;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NotificationManager.Services.Interface.Service
{
    public interface IHttpService
    {
        Task<HttpGeneralResponse> Post(BaseHttpParameter parameters, HttpParametersService httpParametersStrategy);
    }
}
