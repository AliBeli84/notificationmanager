﻿using AutoMapper;
using NotificationManager.Services.Interface.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace NotificationManager.Services.Service
{
    public class BaseService : IDisposable
    {
        protected readonly IUnitOfWork uow;
        protected readonly IMapper _mapper;

        public BaseService(IUnitOfWork uow, IMapper mapper)
        {
            this.uow = uow;
            this._mapper = mapper;
        }

        public void Dispose()
        {
            uow.Dispose();
        }
    }
}
