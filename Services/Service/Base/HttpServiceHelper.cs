﻿using Newtonsoft.Json.Linq;
using NotificationManager.Domain.ViewModels.Common;
using System.Collections.Generic;

namespace NotificationManager.Services.Service.Base
{
    public static class HttpServiceHelper
    {
        public static HttpBaseResponse GetSendResponse(string responseString)
        {
            if (string.IsNullOrWhiteSpace(responseString))
                return new HttpBaseResponse(false, "خطایی رخ داده است.", null);

            return CreateValidSendResponse(JObject.Parse(responseString));
        }

        public static HttpBaseResponse GetStatusResponse(string responseString)
        {
            if (string.IsNullOrWhiteSpace(responseString))
                return new HttpBaseResponse(false, "خطایی رخ داده است.", null);

            return CreateValidStatusResponse(JObject.Parse(responseString));
        }

        public static HttpBaseResponse CreateValidSendResponse(JObject json)
        {
            var result = json.SelectToken("result").ToString();
            var messageids = json.SelectToken("messageids").ToString();

            var message = ValidateSendMessage(messageids);
            var status = (result == "success" && message == null);
            message ??= "پیام ها با موفقیت ارسال شد";
            return new HttpBaseResponse(status, message, messageids);
        }

        public static string ValidateSendMessage(string messageIds)
        {
            var ids = messageIds.Split(",");
            long idNumber = 0;
            string message = null;

            foreach (var id in ids)
            {
                idNumber = long.Parse(id);
                if (idNumber > 1000)
                    continue;
                else
                {
                    message = GetSendValidError(idNumber);
                    break;
                }
            }
            return message;
        }

        public static string GetSendValidError(long id)
        {
            string resultText = id switch
            {
                1 => "نام کاربری یا رمز عبور معتبر نمی باشد .",
                2 => "آرایه ها خالی می باشد.",
                3 => "طول آرایه بیشتر از 100 می باشد .",
                4 => "طول آرایه ی فرستنده و گیرنده و متن پیام با یکدیگر تطابق ندارد .",
                5 => "امکان گرفتن پیام جدید وجود ندارد .",
                6 => "حساب کاربری غیر فعال می باشد.",
                7 => "امکان دسترسی به خط مورد نظر وجود ندارد .",
                8 => "شماره گیرنده نامعتبر است .",
                9 => "حساب اعتبار ریالی مورد نیاز را دارا نمی باشد.",
                10 => "خطایی در سیستم رخ داده است . دوباره سعی کنید .",
                11 => "نامعتبر می باشد . IP",
                20 => "شماره مخاطب فیلتر شده می باشد .",
                21 => "ارتباط با سرویس دهنده قطع می باشد .",
                _ => "خطا رخ داده است.",
            };

            return resultText;
        }

        public static HttpBaseResponse CreateValidStatusResponse(JObject json)
        {
            var result = json.SelectToken("result").ToString();
            var list = json.SelectToken("list").ToString();
            var status = result == "success";

            if (string.IsNullOrWhiteSpace(list))
                return new HttpBaseResponse(false, "اروری رخ داده است", "");

            var message = ValidateStatusMessage(list);
            string text = status ? "وضعیت پیام ها با موفقیت دریافت شد" : "خطایی رخ داده است";
            return new HttpBaseResponse(status, text, message);
        }

        public static string ValidateStatusMessage(string messageIds)
        {
            var ids = messageIds.Split(",");
            long idNumber = 0;
            List<string> result = new List<string>();

            foreach (var id in ids)
            {
                idNumber = long.Parse(id);
                result.Add(GetStatusValidError(idNumber));
            }
            return string.Join(',', result);
        }

        public static string GetStatusValidError(long id)
        {
            string resultText = id switch
            {
                0 => "وضعیت دریافت نشده است(یا پیامک در صف ارسال قرار دارد)",
                1 => "رسیده به گوشی",
                2 => "نرسیده به گوشی",
                8 => "رسیده به مخابرات",
                16 => "نرسیده به مخابرات",
                21 => "شماره گیرنده جزو لیست سیاه می باشد",
                -1 => "شناسه ارسال شده اشتباه است",
                _ => "خطا رخ داده است.",
            };

            return resultText;
        }
    }
}
