﻿using NotificationManager.Domain.ViewModels.Common;
using NotificationManager.Services.Interface.Service;
using NotificationManager.Services.Service.HttpParameters;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace NotificationManager.Services.Service.Common
{
    public class HttpService : IHttpService
    {
        #region == Ctor ==
        private readonly IHttpClientFactory _clientFactory;
        private HttpClient client;

        public HttpService(IHttpClientFactory clientFactory)
        {
            this._clientFactory = clientFactory;
            client = _clientFactory.CreateClient();
        }
        #endregion

        public async Task<HttpGeneralResponse> Post(BaseHttpParameter httpParametere, HttpParametersService httpParametersStrategy)
        {
            var parameters = httpParametersStrategy.GetParameters(httpParametere);
            var data = new FormUrlEncodedContent(parameters);
            client.DefaultRequestHeaders.Add("apikey", httpParametere.ApiKey);
            var responseHTTP = await client.PostAsync(httpParametere.Url, data).ConfigureAwait(false);
            var result = await httpParametersStrategy.GetResponse(responseHTTP);

            if (responseHTTP.IsSuccessStatusCode)
                return new HttpGeneralResponse(result, true);
            else
                return new HttpGeneralResponse(result, false);
        }
    }
}
