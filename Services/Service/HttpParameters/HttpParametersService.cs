﻿using NotificationManager.Domain.ViewModels.Common;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace NotificationManager.Services.Service.HttpParameters
{
    public class HttpParametersService
    {
        private IHttpParametersStrategy _httpParametersStrategy;

        public HttpParametersService(IHttpParametersStrategy httpParametersStrategy)
        {
            _httpParametersStrategy = httpParametersStrategy;
        }

        public Dictionary<string, string> GetParameters(BaseHttpParameter data)
        {
            return _httpParametersStrategy.GetParameter(data);
        }

        public Task<HttpBaseResponse> GetResponse(HttpResponseMessage httpResponseMessage)
        {
            return _httpParametersStrategy.GetResponse(httpResponseMessage);
        }
    }
}
