﻿using NotificationManager.Domain.ViewModels.Common;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace NotificationManager.Services.Service.HttpParameters
{
    public interface IHttpParametersStrategy
    {
        Dictionary<string, string> GetParameter(BaseHttpParameter data);

        Task<HttpBaseResponse> GetResponse(HttpResponseMessage httpResponseMessage);
    }
}
