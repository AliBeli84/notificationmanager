﻿using NotificationManager.Domain.ViewModels.Common;
using NotificationManager.Services.Service.Base;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace NotificationManager.Services.Service.HttpParameters
{
    public class ParsaSmsGroupParameters : IHttpParametersStrategy
    {
        public Dictionary<string, string> GetParameter(BaseHttpParameter httpParameter)
            => new Dictionary<string, string>
            {
                { "message", httpParameter.SmsGroup.Text },
                { "receptor", httpParameter.SmsGroup.PhoneNumbers },
                { "sender", httpParameter.SenderPhone}
            };

        public async Task<HttpBaseResponse> GetResponse(HttpResponseMessage httpResponseMessage)
        {
            var responseString = await httpResponseMessage.Content.ReadAsStringAsync();
            return HttpServiceHelper.GetSendResponse(responseString);
        }
    }
}
