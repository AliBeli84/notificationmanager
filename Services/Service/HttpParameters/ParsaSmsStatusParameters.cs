﻿using NotificationManager.Domain.ViewModels.Common;
using NotificationManager.Services.Service.Base;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace NotificationManager.Services.Service.HttpParameters
{
    public class ParsaSmsStatusParameters : IHttpParametersStrategy
    {
        public Dictionary<string, string> GetParameter(BaseHttpParameter httpParameter)
            => new Dictionary<string, string>
            {
                { "messageids", httpParameter.SmsStatus.MessageIds }
            };

        public async Task<HttpBaseResponse> GetResponse(HttpResponseMessage httpResponseMessage)
        {
            var responseString = await httpResponseMessage.Content.ReadAsStringAsync();
            return HttpServiceHelper.GetStatusResponse(responseString);
        }
    }
}
