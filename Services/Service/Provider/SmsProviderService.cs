﻿using AutoMapper;
using LazyCache;
using NotificationManager.Domain.Constants;
using NotificationManager.Domain.Entities.Provider;
using NotificationManager.Domain.ViewModels.Common;
using NotificationManager.Domain.ViewModels.SmsVM;
using NotificationManager.Services.Interface.Repository;
using NotificationManager.Services.Interface.Service;
using NotificationManager.Services.Service.HttpParameters;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NotificationManager.Services.Service.Provider
{
    #region == Interface ==
    public interface ISmsProviderService
    {
        Task<SmsResponse> SendGroup(SmsGroup smsGroup);
        //Task<SmsResponse> GetHistory(SmsHistoryParameter smsHistoryParameter);
        Task<SmsResponse> GetHistory(bool status);
        Task<SmsResponse> GetSmsStatus(SmsStatus smsStatus);
    }
    #endregion

    #region == Implement ==
    public class SmsProviderService : BaseService, ISmsProviderService
    {
        #region == Ctor ==
        private readonly IHttpService _httpService;
        private readonly IAppCache _cache;

        public SmsProviderService(IUnitOfWork uow, IMapper IMapper,
                    IHttpService httpService, IAppCache cache) : base(uow, IMapper)
        {
            _httpService = httpService;
            _cache = cache;
        }
        #endregion

        #region == GET Method ==
        public async Task<SmsResponse> GetHistory(bool status)
        {
            var smsData = await uow.SmsHistoryRepository.GetAsync<SmsHistoryResult>(i => i.Status == status);
            return new SmsResponse { Status = true, Message = "", Data = smsData };
        }
        #endregion

        #region == Post Method ==
        public async Task<SmsResponse> SendGroup(SmsGroup smsGroup)
        {
            async Task<SmsProvider> smsProvider() => (await uow.SmsProviderRepository.GetAsync(i => i.Provider == smsGroup.Provider)).FirstOrDefault();
            var smsProviderDb = await _cache.GetOrAddAsync(CacheHelper.SmsProvider(smsGroup.Provider), smsProvider, CacheHelper.ExpireDate);
            var httpParameterService = new HttpParametersService(new ParsaSmsGroupParameters());
            var url = smsProviderDb.BaseUrl + ApiUrlHelper.SendBulkSms;
            var httpParameter = new BaseHttpParameter(url, smsProviderDb.Apikey)
            {
                SmsGroup = smsGroup,
                SenderPhone = smsProviderDb.SenderPhone
            };

            var httpResponse = await _httpService.Post(httpParameter, httpParameterService);
            var result = new SmsResponse { Message = httpResponse.Response.Message, Status = httpResponse.Success, Data = httpResponse.Response.Data };

            var phoneNumbers = smsGroup.PhoneNumbers.Split(',');
            if (result.Data != null)
            {
                var msgIds = result.Data.ToString().Split(',');
                for (int i = 0; i < msgIds.Length; i++)
                {
                    SmsHistory smsHistory = new SmsHistory
                    {
                        Message = smsGroup.Text,
                        PhoneNumbers = phoneNumbers[i],
                        Provider = smsGroup.Provider,
                        SendDate = DateTime.Now,
                        Status = httpResponse.Response.Status,
                        MsgId = msgIds[i]
                    };
                    uow.SmsHistoryRepository.Insert(smsHistory);
                }
                await uow.SaveChangesAsync();
            }

            return result;
        }

        public async Task<SmsResponse> GetSmsStatus(SmsStatus smsStatus)
        {
            if (string.IsNullOrWhiteSpace(smsStatus.MessageIds))
                return new SmsResponse { Status = true, Data = null, Message = "" };

            async Task<SmsProvider> smsProvider() => (await uow.SmsProviderRepository.GetAsync(i => i.Provider == smsStatus.Provider)).FirstOrDefault();
            var smsProviderDb = await _cache.GetOrAddAsync(CacheHelper.SmsProvider(smsStatus.Provider), smsProvider, CacheHelper.ExpireDate);
            var httpParameterService = new HttpParametersService(new ParsaSmsStatusParameters());

            var url = smsProviderDb.BaseUrl + ApiUrlHelper.SmsStatus;

            var httpParameter = new BaseHttpParameter(url, smsProviderDb.Apikey)
            {
                SmsStatus = smsStatus
            };

            var httpResponse = await _httpService.Post(httpParameter, httpParameterService);
            return new SmsResponse { Message = httpResponse.Response.Message, Status = httpResponse.Success, Data = httpResponse.Response.Data };
        }
        #endregion
    }
    #endregion
}
