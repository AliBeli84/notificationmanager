﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace NotificationManager.Utilities
{
    public static class IdentityHelper
    {
        public static List<string> AddIdentityErrors(IdentityResult result)
        {
            var errors = new List<string>();
            foreach (var error in result.Errors)
                errors.Add(error.Description);

            return errors;
        }
    }
}
